---
layout: 2020/post
section: propuestas
category: talks
title: Introducción a bases de datos de grafos con Neo4j
state: cancelled
---

Introducción a bases de datos de grafos con ejemplos en vivo usando [Neo4j](https://neo4j.com/).

## Formato de la propuesta

-   [x] &nbsp;Charla (25 minutos)
-   [ ] &nbsp;Charla relámpago (10 minutos)

## Descripción

Se realizará una introducción al funcionamiento de las bases de datos de grafos como alternativa a las bases de datos tradicionales (SQL, documentales...), con ejemplos en vivo usando [Neo4j](https://neo4j.com/), una base de datos [open source](https://github.com/neo4j/neo4j).

Los asistentes podrán tener instalado la herramienta de escritorio para poder probar los ejemplos en sus propios sistemas. (<https://neo4j.com/download/>)

## Público objetivo

No es necesario ningún conocimiento, al ser una introducción, pero se asumirá algún conocimiento básico en otras bases de datos tradicionales.

## Ponente(s)

**Andrés Ortiz**, angry koala.

### Contacto(s)

-   **Andrés Ortiz**: andresortizcorrales at gmail dot com

## Comentarios

Ninguno.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x] &nbsp;Al menos una persona entre los que la proponen estará presente el día programado para la charla.
